module.exports.start = function(connection){
    var collection = connection.collection( 'restaurants' );
    // $match Filters the documents to pass only the documents that 
    // match the specified condition(s) to the next pipeline stage.
    
    // $unwind Deconstructs an array field from the input documents to output a document for each element. Each output document 
    // is the input document with the value of the array field replaced by the element.
    
    // aggr grades of the restaurant based on the borough
    collection.aggregate( 
        [ { '$match': { "borough": "Brooklyn" } },
          { '$unwind': '$grades'},
          { '$group': { '_id': "$grades.grade", 'Sum': { '$sum': 1 } } }		
        ],	  
        function(err, results) {
            console.log(" aggr ....");
            console.log(results)
        }
    );

    collection.count({ 'cuisine': 'Chinese' },	  
        function(err, result) {
            console.log(" count ....");
            console.log(result)
        }
    );

    collection.distinct( 'cuisine', 
        function(err, result) {
            console.log("distinct");
            console.log(result)
        }
    );

    // desc -1 asc 1
    var query = { 'grades.score' : {"$gt": 11}};
    collection.find(query).limit(10).sort({name: -1}).toArray(function(err, result) {
        console.log("find ....");
        if (err) throw err;
        console.log(result);
        result.forEach(function(element, idx) {
            console.log(element);
        }, this);
    });
}