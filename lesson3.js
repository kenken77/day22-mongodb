
module.exports.start = function(connection) {
    // Start Lesson 3
    var col = connection.collection('updates');
    // Insert a single document
    col.insertMany([{a:1}, {a:2}, {a:2}], function(err, r) {
      
      // Update a single document
      col.updateOne({a:1}, {$set: {b: 1}}, function(err, r) {
      
        // Update multiple documents
        col.updateMany({a:2}, {$set: {b: 1}}, function(err, r) {
      
          // Upsert a single document
          col.updateOne({a:3}, {$set: {b: 1}}, {
            upsert: true
          }, function(err, r) {
            console.log(err);  
          });
        });
      });
    }); // End Lesson 3

}