module.exports.start = function(connection) {

    // Start Lesson 1
    
    // Insert a single document
    var insertCollection = connection.collection('inserts');

    insertCollection.insertOne({
        a:1
        , b: function() { return 'hello'; }
        }, {
            w: 'majority'
        , wtimeout: 10000
        , serializeFunctions: true
    }, function(err, r) {
        console.log(err);
    });

    insertCollection.findOne({a:1}, function(err, r){
        console.log("Lesson 1 result : ");
        console.log(r);
    });
    // End Lesson 1
}