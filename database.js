(function(){

const MongoClient = require('mongodb').MongoClient,
    f = require('util').format,
    fs = require('fs');
    const q = require("q");

// Read the certificate authority
var ca = [fs.readFileSync(__dirname + "/certificate/mongodb.pem")];

// Connect validating the returned certificates from the server
const getDB = function(callback){
    MongoClient.connect("mongodb://localhost:27017/test?ssl=true", {
        sslValidate:true,
        sslCA:ca,
        checkServerIdentity:false,
    }, function(err, db) {
        if(err){
            callback(err);
        }else{
            console.log('mongodb connected');
            callback(db);
        }
    });
}


module.exports = { getDB }

})();