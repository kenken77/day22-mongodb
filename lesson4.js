module.exports.start = function(connection){
    var col = connection.collection('removes');
    // Insert a single document
    col.insertMany([{a:1}, {a:2}, {a:2}, {a:3}], function(err, r) {
      
      // Remove a single document
      col.deleteOne({a:1}, function(err, r) {
        // Update multiple documents
        col.deleteMany({a:2}, function(err, r) {
            console.log(err);
        });
      });
    });

    // Modify and return the modified document
    // upsert Perform an upsert operation.
    // Or Update with upsert (Without ObjectId) //This is slow
    //(Find ObjectId's (Slow) -> Not Found -> Insert new document) Else
    //(Find ObjectId's (Slow)-> Found -> Update the documents)
    col.findOneAndUpdate({a:3}, {$set: {b: 4}}, {
        returnOriginal: false
      , upsert: true
    }, function(err, r) {
      console.log(err);
      console.log(r);
    });
}