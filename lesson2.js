var Long = require('mongodb').Long;

module.exports.start = function(connection) {
    
        // Start Lesson 2
        var longValue = Long(1787);
        var longValue2 = Long(4234324234);
        
        // Insert multiple documents
        connection.collection('manynumbers').insertMany([ { a : longValue }, { b : longValue2 } ], function(err, r) {
            console.log(err);
        });
}