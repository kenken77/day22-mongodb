var db = require("./database");
var lesson1 = require("./lesson1");
var lesson2 = require("./lesson2");
var lesson3 = require("./lesson3");
var lesson4 = require("./lesson4");
var lesson5 = require("./lesson5");
var lesson6 = require("./lesson6");

db.getDB(function(connection){
    console.log(connection);
    lesson1.start(connection);
    lesson2.start(connection);
    lesson3.start(connection);
    lesson4.start(connection);
    lesson5.start(connection);
    lesson6.start(connection);
    
    //connection.close();
});